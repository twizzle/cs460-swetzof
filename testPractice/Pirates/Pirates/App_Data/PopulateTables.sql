﻿use [PirateManagement]
--########### Pirates ###########
INSERT INTO [dbo].[Pirates](Name, Date)VALUES('Bad Bart','1999-04-01');
INSERT INTO [dbo].[Pirates](Name, Date)VALUES('Silly Sue','2001-11-01');
INSERT INTO [dbo].[Pirates](Name, Date)VALUES('Grumpy Gus','1970-01-01');
INSERT INTO [dbo].[Pirates](Name, Date)VALUES('Fleeting Fred','1952-02-01');
INSERT INTO [dbo].[Pirates](Name, Date)VALUES('Money Mary','1901-06-01');

-- ########### Ships ###########
INSERT INTO [dbo].[Ships](Name, Type, Tonnage)VALUES('Tidal Wave','Schooner', 126);
INSERT INTO [dbo].[Ships](Name, Type, Tonnage)VALUES('Red Runner','Clipper', 240);
INSERT INTO [dbo].[Ships](Name, Type, Tonnage)VALUES('Purple Haze','Sail Boat', 480);
INSERT INTO [dbo].[Ships](Name, Type, Tonnage)VALUES('Yellow Zombie','Schooner', 170);
INSERT INTO [dbo].[Ships](Name, Type, Tonnage)VALUES('Pink Palace','Clipper', 760);

-- ########### Crews ###########

INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(2,1,10400);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(2,2,1900);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(2,3,200);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(2,4,4010);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(1,1,2125);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(1,2,8015);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(1,3,1215);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(1,5,910);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(3,2,2230);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(3,4,120);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(3,5,3320);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(4,1,215);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(4,2,200);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(4,3,2541);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(4,4,1518);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(4,5,1025);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(5,2,305);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(5,3,1078);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(5,4,9620);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(5,5,525);