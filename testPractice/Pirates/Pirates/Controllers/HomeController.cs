﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pirates.Models;
using Pirates.ViewModel;

namespace Pirates.Controllers
{
    public class HomeController : Controller
    {
        private PirateContext db = new PirateContext();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Ships()
        {
            var ships = db.Ships.ToList();

            return View(ships);
        }

        public ActionResult Crews()
        {
            var crews = db.Crews.ToList();

            return View(crews);
        }

        public ActionResult CrewGroup()
        {
            IQueryable<PirateBooty> data = from pirate in db.Pirates
                                           group pirate by pirate.Name into pirateIDgroup
                                           select new PirateBooty()
                                           {
                                               PirateName = pirateIDgroup.Key,
                                               TotalBooty = pirateIDgroup.Sum(p => p.Crews.Sum(b => b.
                                                 BOOTYVALUE)),
                                           };
            return View(data.OrderBy(b => b.TotalBooty).ToList());
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}