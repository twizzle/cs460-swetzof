﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3
{
    /// <summary>
    /// Using a LinkedStack, performs postfix calculations
    /// </summary>
    class Calculator
    {
        /// <summary>
        /// data structure used to hold operands for the postfix calculation.
        /// </summary>
        /// 
        private StackADT stack = new ILinkedStack();

        /// <summary>
        /// Entry point for the application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //Instantiate a new Calculator object
            Calculator app = new HW3.Calculator();
            Boolean playAgain = true;

            Console.WriteLine("Postfix Calculator.  Recognizes these operators:  + - * /");
            while (playAgain)
            {
                playAgain = app.DoCalculation();
            }
            Console.WriteLine("Good Bye!");
        }

        /// <summary>
        /// Sets up the calculation, and a place for the user to enter information.
        /// </summary>
        /// <returns></returns>
        private Boolean DoCalculation()
        {
            Console.WriteLine("Please enter q to quit");
            string input = "2 2 +";
            Console.Write(">>> ");

            input = Console.ReadLine();
            if (input.StartsWith("q") || input.StartsWith("Q"))
            {
                return false;
            }

            string output = "4";

            try
            {
                output = EvlauatePostFixInput(input);
            }

            catch (Exception e)
            {
                output = "Illegal Argument Exception";
            }

            Console.WriteLine(">>> " + input + " = " + output);

            return true;
        } 
        
        /// <summary>
        /// Parses the user input, and sends to calculation method.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public string EvlauatePostFixInput(string input)
        {
            if (input == null || input.Equals(""))
            {
                Console.WriteLine("Null or the empty strings are not valid postfix expressions");
            }

            //clean stack and start again
            stack.Clear();

            string st; //original input
            string s; // operator
            double a; //operand
            double b;//operand
            double c;//answer
            double temp;

            st = input;
            string[] tokens = st.Split(' ');

            for (int i=0; i<tokens.Length; i++)
            {
                if (Double.TryParse(tokens[i], out temp))
                {
                    stack.Push(temp);
                }
                else
                {
                    s = tokens[i];
                    if (s.Length > 1)
                    {
                        throw new ArgumentException("Input Error:  " + s + " is not an allowed number or operator");
                    }
                    if (stack.IsEmpty())
                    {
                        throw new ArgumentException("Improper input format.  Stack became empty when expecting second operand");
                    }

                    b = ((Double)stack.Pop());
                    a = ((Double)stack.Pop());
                    c = DoOperation(a, b, s);
                    stack.Push(c);
                }//end else

            }//end for loop
            
            return (stack.Pop().ToString());
        }

        /// <summary>
        /// Performs the postfix calculation based on user input
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public double DoOperation(double a, double b, string s)
        {
            double c = 0.0;
            if (s.Equals("+"))
                c = (a + b);
            else if (s.Equals("-"))
                c = (a - b);
            else if (s.Equals("*"))
                c = (a * b);
            else if (s.Equals("/"))
                try
                {
                    c = (a / b);
                    if (c == Double.NegativeInfinity || c == Double.PositiveInfinity)
                        throw new Exception("Can't divide by zero");
                }
                catch (ArithmeticException e)
                {
                    throw new Exception(e.ToString());
                }
            else throw new Exception("Improper operator: : " + s + ", is not one of +, -, *, or /");
               
            return c;
        }


    }
}
