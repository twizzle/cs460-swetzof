﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3
{
    /// <summary>
    /// Node.cs provides methods for nodes to be used by the StackADT.
    /// </summary>

    class Node
    {

        public object Data { get; set; }
        public Node Next { get; set; }

    }
}
