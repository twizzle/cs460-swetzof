﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3
{
    /// <summary>
    /// This class is designed to be an interface for a stack
    /// 
    /// </summary>
    interface StackADT
    {
        /// <summary>
        /// Push an object to the top of a stack
        /// </summary>
        /// <param name="newItem"></param>
        /// <returns></returns>
        object Push(object newItem);

        /// <summary>
        /// Take an object off of the top of the stack
        /// </summary>
        /// <returns></returns>
        object Pop();

        /// <summary>
        /// View the items on the stack
        /// </summary>
        /// <returns></returns>
        object Peek();

        /// <summary>
        /// Check to see if the stack is empty
        /// </summary>
        /// <returns></returns>
        Boolean IsEmpty();

        /// <summary>
        /// remove all items from the stack
        /// </summary>
        void Clear();
    }
}
