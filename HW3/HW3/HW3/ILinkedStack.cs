﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3
{
    /// <summary>
    /// A singly linked stack implementation
    /// </summary>
    class ILinkedStack : StackADT
    {
        private Node top;

        /// <summary>
        /// Instantiates a LinkedStack.
        /// </summary>
        public ILinkedStack()
        {
            top = null; // Empty stack condition
        }

        /// <summary>
        /// Takes an object, pushes to top of stack, resets top
        /// </summary>
        /// <param name="newItem"></param>
        /// <returns></returns>
        public object Push(object newItem)
        {
            if (newItem == null)
            {
                return null;
            }

            Node newNode = new Node();
            newNode.Data = newItem;
            newNode.Next = top;
            top = newNode;
            return newItem;
        }

        /// <summary>
        /// Removes and returns the object at the top of the stack.
        /// </summary>
        /// <returns></returns>
        public object Pop()
        {
            if (IsEmpty())
            {
                return null;
            }

            object topItem = top.Data;
            top = top.Next;
            return topItem;
        }

        /// <summary>
        /// Returns the value of the Object at the top of the stack.
        /// </summary>
        /// <returns></returns>
        public object Peek()
        {
            if (IsEmpty())
            {
                return null;
            }

            return top.Data;
        }

        /// <summary>
        /// Checks to see if the top of the stack is null.
        /// </summary>
        /// <returns></returns>
        public Boolean IsEmpty()
        {
            return top == null;
        }

        /// <summary>
        /// Sets the top of the stack to null, which clears the stack.
        /// </summary>
        public void Clear()
        {
            top = null;
        }

    }
}
