﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StockInfo.Models;

namespace StockInfo.Controllers
{
    public class UserLogController : Controller
    {
        private UserContext db = new UserContext();

        // GET: UserLog
        public ActionResult Index()
        {
            return View(db.UserLogs.ToList());
        }

        // GET: UserLog/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserLog userLog = db.UserLogs.Find(id);
            if (userLog == null)
            {
                return HttpNotFound();
            }
            return View(userLog);
        }

        // GET: UserLog/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserLog/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,STOCKSYMBOL,IPADDRESS,BROWSER,TIMESTAMP")] UserLog userLog)
        {
            if (ModelState.IsValid)
            {
                db.UserLogs.Add(userLog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(userLog);
        }

        // GET: UserLog/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserLog userLog = db.UserLogs.Find(id);
            if (userLog == null)
            {
                return HttpNotFound();
            }
            return View(userLog);
        }

        // POST: UserLog/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,STOCKSYMBOL,IPADDRESS,BROWSER,TIMESTAMP")] UserLog userLog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userLog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userLog);
        }

        // GET: UserLog/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserLog userLog = db.UserLogs.Find(id);
            if (userLog == null)
            {
                return HttpNotFound();
            }
            return View(userLog);
        }

        // POST: UserLog/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserLog userLog = db.UserLogs.Find(id);
            db.UserLogs.Remove(userLog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
