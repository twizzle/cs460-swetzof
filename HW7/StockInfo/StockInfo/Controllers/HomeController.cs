﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StockInfo.Models;

namespace StockInfo.Controllers
{
    public class HomeController : Controller
    {
        private UserContext db = new UserContext();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetStockInfo(string stockSymbol)
        {

            var data = "test"; //finish this go to yahoo for csv file

            return Json(data, JsonRequestBehavior.AllowGet );
        }

        [HttpPost]
        public ActionResult Index(string stockSymbol, string userBrowser, string userIP, DateTime dateStamp)
        {
            LogInfo(stockSymbol, userBrowser, userIP, dateStamp);
            var stockResult = GetStockInfo(stockSymbol);
            return View(stockResult);
        }

        [HttpPost]
        public ActionResult LogInfo(string stockSymbol, string userBrowswer, string userIP, DateTime dateStamp)
        {
            UserLog user = new UserLog();
            user.STOCKSYMBOL = stockSymbol;
            user.BROWSER = userBrowswer;
            user.IPADDRESS = userIP;
            user.TIMESTAMP = dateStamp;

            if (ModelState.IsValid)
            {
                db.UserLogs.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(); ;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}