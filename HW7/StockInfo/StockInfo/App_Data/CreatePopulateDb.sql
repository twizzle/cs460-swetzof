﻿CREATE DATABASE [StockManagement] ON  PRIMARY 
    ( NAME = N'StockManagement', FILENAME = N'c:\Users\twizz_000\Documents\WOU\CS460\Homework\HW7\StockInfo\StockInfo\App_Data\StockManagement.mdf' , SIZE = 167872KB , MAXSIZE = UNLIMITED, FILEGROWTH = 16384KB )
     LOG ON 
    ( NAME = N'StockManagement_Log', FILENAME =  N'c:\Users\twizz_000\Documents\WOU\CS460\Homework\HW7\StockInfo\StockInfo\App_Data\StockManagement_log.ldg' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 16384KB )
    GO

USE [StockManagement];
GO

-- *********** Drop Tables ***********
IF OBJECT_ID('dbo.UserLog','U') IS NOT NULL
	DROP TABLE [dbo].[UserLog];
GO


--########### Pirates ###########
CREATE TABLE [dbo].[UserLog]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[STOCKSYMBOL] NVARCHAR (50) NOT NULL,
	[IPADDRESS] NVARCHAR (50) NOT NULL,
	[BROWSER] NVARCHAR (50) NOT NULL,
	[TIMESTAMP] DATETIME NOT NULL,
	CONSTRAINT [PK_dbo.UserLog] PRIMARY KEY CLUSTERED ([ID] ASC)
);
