namespace StockInfo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserLog")]
    public partial class UserLog
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string STOCKSYMBOL { get; set; }

        [Required]
        [StringLength(50)]
        public string IPADDRESS { get; set; }

        [Required]
        [StringLength(50)]
        public string BROWSER { get; set; }

        public DateTime TIMESTAMP { get; set; }
    }
}
