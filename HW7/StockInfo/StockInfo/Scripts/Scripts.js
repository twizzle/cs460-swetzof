﻿

// Ajax call to get stock information
$("#Submit").click(function () {
    var stockSymbol = $("#Stock").val();
    var userBrowser = $(navigator.appName.toString());
    var userIP = $( $.get('http://jsonip.com/', function(r){ console.log(r.ip); }));
    var dateStamp = $(new Date()).toString();
    console.log(stockSymbol);
    console.log(userBrowser);
    console.log(userIP);
    console.log(dateStamp);

    var source = "/Home/GetStockInfo/" + "?" + stockSymbol + "?" + userBrowser + "?" + userIP + "?" + dateStamp;

    console.log(source);

    // get data from Home Controller GetStockInfo
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: displayData
    });
});

function displayData(data) {
    $("#results").text("Your stock symbol, browswer, IP address and date stamp have been recorded");
}