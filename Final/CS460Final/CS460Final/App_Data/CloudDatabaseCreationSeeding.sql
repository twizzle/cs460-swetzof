﻿IF OBJECT_ID('dbo.Crews','U') IS NOT NULL
	DROP TABLE [dbo].[Crews];
GO

IF OBJECT_ID('dbo.Pirates','U') IS NOT NULL
	DROP TABLE [dbo].[Pirates];
GO

IF OBJECT_ID('dbo.Ships','U') IS NOT NULL
	DROP TABLE [dbo].[Ships];
GO

--########### Pirates ###########
CREATE TABLE [dbo].[Pirates]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Name] NVARCHAR (50) NOT NULL,
	[Date] DATETIME NOT NULL,
	CONSTRAINT [PK_dbo.Pirates] PRIMARY KEY CLUSTERED ([ID] ASC)
);

--#################Ships################
CREATE TABLE [dbo].[Ships]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Name] NVARCHAR (50) NOT NULL,
	[TYPE] NVARCHAR (50) NOT NULL,
	[TONNAGE] INT NOT NULL,
	Constraint [PK_dbo.Ships] PRIMARY KEY CLUSTERED ([ID] ASC),
);

--#################Crew#################
CREATE TABLE [dbo].[Crews]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[PIRATEID] INT NOT NULL REFERENCES Pirates ([ID]),
	[SHIPID] INT NOT NULL REFERENCES Ships ([ID]),
	[BOOTYVALUE] DECIMAL (7,2)NOT NULL,
	Constraint [PK_dbo.Crews] PRIMARY KEY CLUSTERED ([ID] ASC)
);

--########### Pirates ###########
INSERT INTO [dbo].[Pirates](Name, Date)VALUES('Bad Bart','1999-04-01');
INSERT INTO [dbo].[Pirates](Name, Date)VALUES('Silly Sue','2001-11-01');
INSERT INTO [dbo].[Pirates](Name, Date)VALUES('Grumpy Gus','1970-01-01');
INSERT INTO [dbo].[Pirates](Name, Date)VALUES('Fleeting Fred','1952-02-01');
INSERT INTO [dbo].[Pirates](Name, Date)VALUES('Money Mary','1901-06-01');

-- ########### Ships ###########
INSERT INTO [dbo].[Ships](Name, Type, Tonnage)VALUES('Tidal Wave','Schooner', 126);
INSERT INTO [dbo].[Ships](Name, Type, Tonnage)VALUES('Red Runner','Clipper', 240);
INSERT INTO [dbo].[Ships](Name, Type, Tonnage)VALUES('Purple Haze','Sail Boat', 480);
INSERT INTO [dbo].[Ships](Name, Type, Tonnage)VALUES('Yellow Zombie','Schooner', 170);
INSERT INTO [dbo].[Ships](Name, Type, Tonnage)VALUES('Pink Palace','Clipper', 760);

-- ########### Crews ###########

INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(2,1,10400);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(2,2,1900);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(2,3,200);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(2,4,4010);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(1,1,2125);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(1,2,8015);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(1,3,1215);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(1,5,910);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(3,2,2230);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(3,4,120);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(3,5,3320);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(4,1,215);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(4,2,200);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(4,3,2541);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(4,4,1518);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(4,5,1025);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(5,2,305);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(5,3,1078);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(5,4,9620);
INSERT INTO [dbo].[Crews](PirateID, ShipID, BootyValue)VALUES(5,5,525);