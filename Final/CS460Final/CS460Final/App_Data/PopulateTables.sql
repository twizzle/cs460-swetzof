﻿use [ArtManagement.MDF]
--###########  Artists  ###########
INSERT INTO [dbo].[Artists](NAME, BIRTHDATE, BIRTHCITY)VALUES('M.C. Escher',1898-06-17, 'Leewarden, Netherlands');
INSERT INTO [dbo].[Artists](NAME, BIRTHDATE, BIRTHCITY)VALUES('Leonardo Da Vinci',1519-05-02, 'Vinci, Italy');
INSERT INTO [dbo].[Artists](NAME, BIRTHDATE, BIRTHCITY)VALUES('Hatip Mehmed Efendi',1680-11-18, 'Unknown');
INSERT INTO [dbo].[Artists](NAME, BIRTHDATE, BIRTHCITY)VALUES('Salvador Dali',1904-05-11, 'Figueres, Spain');

-- ########### Genres ###########
INSERT INTO [dbo].[Genres](Name)VALUES('Tesselation');
INSERT INTO [dbo].[Genres](Name)VALUES('Surrealism');
INSERT INTO [dbo].[Genres](Name)VALUES('Portrait');
INSERT INTO [dbo].[Genres](Name)VALUES('Renaissance');

-- ########### ArtWork ###########

INSERT INTO [dbo].[ArtWork](ARTISTID, TITLE)VALUES(1, 'Circle Limit III');
INSERT INTO [dbo].[ArtWork](ARTISTID, TITLE)VALUES(1, 'Twon Tree');
INSERT INTO [dbo].[ArtWork](ARTISTID, TITLE)VALUES(2, 'Mona Lisa');
INSERT INTO [dbo].[ArtWork](ARTISTID, TITLE)VALUES(2, 'The Vitruvian Man');
INSERT INTO [dbo].[ArtWork](ARTISTID, TITLE)VALUES(3, 'Ebru');
INSERT INTO [dbo].[ArtWork](ARTISTID, TITLE)VALUES(4, 'Honey is Sweeter Than Blood');

-- ########### Classification ###########

INSERT INTO [dbo].[Classification](ARTWORKID, GENREID)VALUES(1, 1);
INSERT INTO [dbo].[Classification](ARTWORKID, GENREID)VALUES(2, 1);
INSERT INTO [dbo].[Classification](ARTWORKID, GENREID)VALUES(2, 2);
INSERT INTO [dbo].[Classification](ARTWORKID, GENREID)VALUES(3, 3);
INSERT INTO [dbo].[Classification](ARTWORKID, GENREID)VALUES(3, 4);
INSERT INTO [dbo].[Classification](ARTWORKID, GENREID)VALUES(4, 4);
INSERT INTO [dbo].[Classification](ARTWORKID, GENREID)VALUES(5, 1);
INSERT INTO [dbo].[Classification](ARTWORKID, GENREID)VALUES(6, 2);