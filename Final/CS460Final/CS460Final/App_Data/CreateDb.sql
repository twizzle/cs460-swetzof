﻿
-- Create tables and populate with seed data
--    follow naming convention: "Users" table contains rows that are each "User" objects

-- ***********  Attach **********
CREATE DATABASE [ArtManagement] ON  PRIMARY 
    ( NAME = N'ArtManagement', FILENAME = N'C:\Users\twizz_000\Documents\WOU\CS460\Homework\Final\CS460Final\CS460Final\App_Data\ArtManagement.mdf' , SIZE = 167872KB , MAXSIZE = UNLIMITED, FILEGROWTH = 16384KB )
     LOG ON 
    ( NAME = N'ArtManagement_log', FILENAME = N'C:\Users\twizz_000\Documents\WOU\CS460\Homework\Final\CS460Final\CS460Final\App_Data\ArtManagement_log.ldg' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 16384KB )
    GO

USE [ArtManagement];
GO

-- *********** Drop Tables ***********
IF OBJECT_ID('dbo.Classifications','U') IS NOT NULL
	DROP TABLE [dbo].[Classifications];
GO

IF OBJECT_ID('dbo.ArtWork','U') IS NOT NULL
	DROP TABLE [dbo].[Artwork];
GO

IF OBJECT_ID('dbo.Artists','U') IS NOT NULL
	DROP TABLE [dbo].[Artists];
GO

IF OBJECT_ID('dbo.Classifications','U') IS NOT NULL
	DROP TABLE [dbo].[Classifications];
GO

IF OBJECT_ID('dbo.Genres','U') IS NOT NULL
	DROP TABLE [dbo].[Genres];
GO

--########### Artists ###########
CREATE TABLE [dbo].[Artists]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[NAME] NVARCHAR (50) NOT NULL,
	[BIRTHDATE] DATETIME NOT NULL,
	[BIRTHCITY] NVARCHAR (50) NOT NULL,
	CONSTRAINT [PK_dbo.Artists] PRIMARY KEY CLUSTERED ([ID] ASC)
);

--################# Genres ################
CREATE TABLE [dbo].[Genres]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[NAME] NVARCHAR (50) NOT NULL,
	Constraint [PK_dbo.Genres] PRIMARY KEY CLUSTERED ([ID] ASC),
);

--################# Artwork #################
CREATE TABLE [dbo].[Artwork]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[ARTISTID] INT NOT NULL REFERENCES Artists ([ID]),
	[TITLE] NVARCHAR (50) NOT NULL,
	Constraint [PK_dbo.Artwork] PRIMARY KEY CLUSTERED ([ID] ASC),
	);

	--################# Classification #################
CREATE TABLE [dbo].[Classification]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[ARTWORKID] INT NOT NULL REFERENCES Artwork ([ID]),
	[GENREID] INT NOT NULL REFERENCES Genres ([ID]),
	Constraint [PK_dbo.Crews] PRIMARY KEY CLUSTERED ([ID] ASC)
);