﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS460Final.ModelView
{
    public class ArtistGenre
    {
   
            public int ID { get; set; }
            public int GenreID { get; set; }
            public string ArtistName { get; set; }
            public string Title { get; set; }

    }
}