﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CS460Final.Models;
using CS460Final.ModelView;

namespace CS460Final.Controllers
{
    public class HomeController : Controller
    {
        private ArtContext db = new ArtContext();

        // GET: Home
        public ActionResult Index()
        {
            var genres = db.Genres.ToList();
            return View(genres);
        }

        public ActionResult ArtWork()
        {
            var artwork = db.Artworks;
            return View(artwork);
        }

        public ActionResult Classifcations()
        {
            var classification = db.Classifications;
            return View(classification);
        }

        public ActionResult GetGenre(int? id)
        {

            return View();
        }

        private IQueryable<ArtistGenre> GetData(int genreID)
        {
            int id = Convert.ToInt32(genreID);
            IQueryable<ArtistGenre> data =
                from a in db.Artists
                join aw in db.Artworks on a.ID equals aw.ARTISTID
                join c in db.Classifications on aw.ID equals c.ARTWORKID
                where c.GENREID == id
                select new ArtistGenre { ArtistName = a.NAME, Title = aw.TITLE };

            return data;
        }

        public JsonResult GetArtwork(int id)
        {

            IQueryable<ArtistGenre> data = GetData(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Index(int id)
        {
            var genreList = GetArtwork(id);
            return View(genreList);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }



    }
}