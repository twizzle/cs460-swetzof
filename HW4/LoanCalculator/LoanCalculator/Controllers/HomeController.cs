﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoanCalculator.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        /// <summary>
        /// 
        /// Entry point for the home page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Using Request.QueryString to get the entries for the loan application
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LoanApp()
        {
            var FirstName = Request.QueryString["FirstName"];
            var LastName = Request.QueryString["LastName"];
            var Employer = Request.QueryString["Employer"];

            return View() ;
        }
        /// <summary>
        /// Posts the entries from the Loan application and returns a new view "Response"
        /// </summary>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="Employer"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoanApp(string FirstName, string LastName, string Employer)
        {
            ViewBag.message=("Thank you, " + FirstName + " " + LastName + ".  We will contact " + Employer + " to verify your employment, and then be in touch with you.");

            return View("Response");
        }

        /// <summary>
        /// Gets the data from the ResetPin view form
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ResetPin()
        {
            return View();
        }

        /// <summary>
        /// Posts the results from the form on the reset pin page, returns the changePinConfirmation page
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ResetPin(FormCollection form)
        {

           ViewBag.message = "Thank you, " + form["UserName"] + " for changing your PIN, your old PIN is:  " + form["OldPin"] + " and your new PIN is:  " + form["NewPin"];
            return View("PinChangeConfirmation");
        }

        /// <summary>
        /// gets the form for the loan calculator
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LoanCalc()
        {
            return View();
        }

        /// <summary>
        /// Posts the entries from the loan calculator form, returns a PaymentAmount view
        /// </summary>
        /// <param name="LoanAmount"></param>
        /// <param name="Interest"></param>
        /// <param name="Months"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoanCalc(double LoanAmount, double Interest, int Months)
        {
            var interestRate = Interest / 1200;
            var term = -Months;
            var expo = Math.Pow((1 + interestRate), term);
            var MPayment =(interestRate * LoanAmount) / (1 - expo);
            var TotalCost =MPayment * Months;
            var TotalInterest = TotalCost - LoanAmount;

            ViewBag.MonthlyPayment = "Total Interest is: $" + TotalInterest.ToString("N2") + ", Total Cost is: $" + TotalCost.ToString("N2") + ", and Monthly Payment is: $" + MPayment.ToString("N2");
            return View("PaymentAmount");
        }
    }
}