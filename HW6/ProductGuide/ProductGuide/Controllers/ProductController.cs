﻿using ProductGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace ProductGuide.Controllers
{
    public class ProductController : Controller
    {
        private ProductContext db = new ProductContext();
        // GET: Product
        /// <summary>
        /// Gets the set of main categories for the view
        /// </summary>
        /// <returns>Index View</returns>
        public ActionResult Index()
        {
            return View(db.ProductCategories.ToList());
        }

        /// <summary>
        /// After selection of a category, redirects to Subcategories with the CategoryID
        /// using form collection to get ProductCategoryID
        /// </summary>
        /// <param name="fc"></param>
        /// <returns>SubCategories View</returns>
        [HttpPost]
        public ActionResult Index(FormCollection fc)
        {
            var categoryId = Convert.ToInt32(fc["item.ProductCategoryID"]);
            ViewBag.catId = categoryId;           
            return RedirectToAction("SubCategories",  new { id = categoryId });
        }

       //GET:  SubCategory
       /// <summary>
       /// Takes the ID and returns the view of subcategories with that ID
       /// </summary>
       /// <param name="id"></param>
       /// <returns>Subcategory</returns>
        public ActionResult SubCategories(int? id)
        {
            var subCategory = db.ProductSubcategories.Where(d => d.ProductCategoryID == id).ToList();
            return View(subCategory);
        }
        /// <summary>
        /// After Subcategory is selected uses form collection to Return a list of products that match the category and subcategory
        /// </summary>
        /// <param name="fc"></param>
        /// <returns>Product View</returns>
        [HttpPost]
        public ActionResult SubCategories(FormCollection fc)
        {
            var subCatId = Convert.ToInt32(fc["item.ProductSubcategoryID"]);
            ViewBag.subCatId = subCatId;
            return RedirectToAction("Product", new { subId = subCatId }) ;
        }
        //GET:  Product
        /// <summary>
        /// Accepts a subcategoryID and page number for paging purposes
        /// Paging capabilities used from  Contoso University Tutorial at
        /// https://www.asp.net/mvc/overview/getting-started/getting-started-with-ef-using-mvc/creating-an-entity-framework-data-model-for-an-asp-net-mvc-application
        /// </summary>
        /// <param name="subId"></param>
        /// <param name="page"></param>
        /// <returns>Paged List View of Product</returns>
        public ActionResult Product(int? subId, int? page)
        {
            if (page == null)
                page = 1;
            var product = db.Products.Where(p => p.ProductSubcategoryID == subId).ToList();
            //page = 1;
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(product.ToPagedList(pageNumber, pageSize));
        }

        //GET:  Product Details
        /// <summary>
        /// After selecting a link on the Product page, redirects to the details page.  Review is accessed from here.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Details View</returns>
        public ActionResult Details(int? id)
        {
            var product = db.Products.Find(id);
            return View(product);
        }
        //GET:  Review 
        /// <summary>
        /// Review form to leave a review for the product that is on the screen, user fills out their information
        /// other data is auto-filled.  Accepts the ProductID as a parameter.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Review(int? id)
        {
            ViewBag.Id = id;
            var ProductID = id;
            return View();
        }

        /// <summary>
        /// After form is filled out, receives and posts the data to the ProductReview table, if error, returns
        /// to the form.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="productReview"></param>
        /// <returns>Redirects to Confirmation</returns>
        [HttpPost]
        public ActionResult Review(int? id, ProductReview productReview )
        {
            productReview.ProductID = (int)id;
            productReview.ReviewDate = DateTime.Today;
            productReview.ModifiedDate = DateTime.Today;
     
            if (ModelState.IsValid)
            {
                db.ProductReviews.Add(productReview);
                db.SaveChanges();
                return RedirectToAction("Confirmation");
            }
            return View(productReview);
        }
        //GET:  Confirmation
        /// <summary>
        /// Displays confirmation for review form
        /// </summary>
        /// <returns>Confirmation</returns>
        public ActionResult Confirmation()
        {
            return View();
        }
 
    }
}