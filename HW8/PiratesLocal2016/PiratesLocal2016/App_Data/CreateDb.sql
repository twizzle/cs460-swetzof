﻿
-- Create tables and populate with seed data
--    follow naming convention: "Users" table contains rows that are each "User" objects

-- ***********  Attach **********
CREATE DATABASE [PirateManagement] ON  PRIMARY 
    ( NAME = N'PirateManagement', FILENAME = N'c:\Users\twizz_000\Documents\WOU\CS460\Homework\HW8\PiratesLocal2016\PiratesLocal2016\App_Data\PirateManagement.mdf' , SIZE = 167872KB , MAXSIZE = UNLIMITED, FILEGROWTH = 16384KB )
     LOG ON 
    ( NAME = N'PirateManagement_Log', FILENAME = N'c:\Users\twizz_000\Documents\WOU\CS460\Homework\HW8\PiratesLocal2016\PiratesLocal2016\App_Data\PirateManagement_log.ldg' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 16384KB )
    GO

USE [PirateManagement];
GO

-- *********** Drop Tables ***********
IF OBJECT_ID('dbo.Crews','U') IS NOT NULL
	DROP TABLE [dbo].[Crews];
GO

IF OBJECT_ID('dbo.Pirates','U') IS NOT NULL
	DROP TABLE [dbo].[Pirates];
GO

IF OBJECT_ID('dbo.Ships','U') IS NOT NULL
	DROP TABLE [dbo].[Ships];
GO

--########### Pirates ###########
CREATE TABLE [dbo].[Pirates]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Name] NVARCHAR (50) NOT NULL,
	[Date] DATETIME NOT NULL,
	CONSTRAINT [PK_dbo.Pirates] PRIMARY KEY CLUSTERED ([ID] ASC)
);

--#################Ships################
CREATE TABLE [dbo].[Ships]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Name] NVARCHAR (50) NOT NULL,
	[TYPE] NVARCHAR (50) NOT NULL,
	[TONNAGE] INT NOT NULL,
	Constraint [PK_dbo.Ships] PRIMARY KEY CLUSTERED ([ID] ASC),
);

--#################Crew#################
CREATE TABLE [dbo].[Crews]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[PIRATEID] INT NOT NULL REFERENCES Pirates ([ID]),
	[SHIPID] INT NOT NULL REFERENCES Ships ([ID]),
	[BOOTYVALUE] DECIMAL (7,2)NOT NULL,
	Constraint [PK_dbo.Crews] PRIMARY KEY CLUSTERED ([ID] ASC)
);