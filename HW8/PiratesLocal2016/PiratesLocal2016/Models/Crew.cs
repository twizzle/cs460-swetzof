namespace PiratesLocal2016.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Crew
    {
        /// <summary>
        /// Gets and sets fields for the Crew, foreign keys in Pirate and Ship
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// FK from Pirates Table
        /// </summary>
        public int PIRATEID { get; set; }
        /// <summary>
        /// FK from Ships Table
        /// </summary>
        public int SHIPID { get; set; }

        public decimal BOOTYVALUE { get; set; }

        public virtual Pirate Pirate { get; set; }

        public virtual Ship Ship { get; set; }
    }
}
