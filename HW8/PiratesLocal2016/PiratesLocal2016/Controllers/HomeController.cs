﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PiratesLocal2016.Models;
using PiratesLocal2016.ViewModels;

namespace PiratesLocal2016.Controllers
{
    public class HomeController : Controller
    {
        private PirateContext db = new PirateContext();

        // GET: Home
        /// <summary>
        /// Gets the Home Index page
        /// </summary>
        /// <returns>Index View</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Gets a list ofships
        /// </summary>
        /// <returns>Ship List View</returns>
        public ActionResult Ship()
        {
            var ships = db.Ships.ToList();
            return View(ships);
        }

        /// <summary>
        /// Gets a list of the crews
        /// </summary>
        /// <returns> Crew List View</returns>
        public ActionResult Crew()
        {
            var crews = db.Crews.ToList();
            //var crews = db.Crews.GroupBy(p => p.Pirate).ToList();
            return View(crews);
        }

        /// <summary>
        /// gets the pirate booty data
        /// </summary>
        /// <returns>data</returns>
        private IQueryable<PirateBooty> GetData()
        {
            IQueryable<PirateBooty> data = 
                from pirate in db.Pirates
                   group pirate by pirate.Name into pirateIDgroup
                   select new PirateBooty()
                   {
                       PirateName = pirateIDgroup.Key,
                       TotalBooty = pirateIDgroup.Sum(p => p.Crews.Sum(b => b.BOOTYVALUE)),
                   };
            //data.OrderBy(b => b.TotalBooty);
            return data.OrderBy(b => b.TotalBooty) ;
        }

        /// <summary>
        /// calls GetData, returns the Json Request
        /// </summary>
        /// <returns>Json data</returns>
        public JsonResult GetPirateBootyData()
        {
            IQueryable<PirateBooty> data = GetData();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// called when crewgroup is loaded, calls GetPirateBootyData
        /// </summary>
        /// <returns>Json piratebooty</returns>
        public ActionResult CrewGroup()
        {
            var pirateBooty = GetPirateBootyData();
            return View (pirateBooty);
        }

        /// <summary>
        /// Disposes of the db
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


}
}