﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PiratesLocal2016.Models;
using PagedList;

namespace PiratesLocal2016.Controllers
{
    public class PirateController : Controller
    {
        private PirateContext db = new PirateContext();

        /// <summary>
        /// Gets the Pirate List with page numbers
        /// </summary>
        /// <param name="page"></param>
        /// <returns> Pirates Index paged list view</returns>
        // GET: Pirate
        public ActionResult Index(int? page)
        {
            if (page == null)
                page = 1;
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            var pirates = db.Pirates.ToList();

            return View(pirates.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Gets the details for a selected pirate
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Details View</returns>
        // GET: Pirate/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pirate pirate = db.Pirates.Find(id);
            if (pirate == null)
            {
                return HttpNotFound();
            }
            return View(pirate);
        }

        /// <summary>
        /// Gets a blank create pirate form
        /// </summary>
        /// <returns>Create view</returns>
        // GET: Pirate/Create
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Saves the created pirate to db, if validation met
        /// otherwise returns to form
        /// </summary>
        /// <param name="pirate"></param>
        /// <returns></returns>
        // POST: Pirate/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Date")] Pirate pirate)
        {
            if (ModelState.IsValid)
            {
                db.Pirates.Add(pirate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pirate);
        }

        /// <summary>
        /// Gets edit form for selected pirate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Pirate/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pirate pirate = db.Pirates.Find(id);
            if (pirate == null)
            {
                return HttpNotFound();
            }
            return View(pirate);
        }

        /// <summary>
        /// Saves pirate to db if valid, else returns to form
        /// </summary>
        /// <param name="pirate"></param>
        /// <returns></returns>
        // POST: Pirate/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Date")] Pirate pirate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pirate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pirate);
        }

        /// <summary>
        /// Gets delete form for selected pirate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Pirate/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pirate pirate = db.Pirates.Find(id);
            if (pirate == null)
            {
                return HttpNotFound();
            }
            return View(pirate);
        }

        /// <summary>
        /// Deletes selected pirate and redirects to Index
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // POST: Pirate/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pirate pirate = db.Pirates.Find(id);
            db.Pirates.Remove(pirate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Disposes of db
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
