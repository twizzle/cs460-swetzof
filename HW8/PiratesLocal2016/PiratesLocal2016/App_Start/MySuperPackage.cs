using System;

[assembly: WebActivator.PreApplicationStartMethod(
    typeof(PiratesLocal2016.App_Start.MySuperPackage), "PreStart")]

namespace PiratesLocal2016.App_Start {
    public static class MySuperPackage {
        public static void PreStart() {
            MVCControlsToolkit.Core.Extensions.Register();
            System.Web.Mvc.GlobalFilters.Filters.Add(new MVCControlsToolkit.ActionFilters.PlaceJavascriptAttribute());
        }
    }
}