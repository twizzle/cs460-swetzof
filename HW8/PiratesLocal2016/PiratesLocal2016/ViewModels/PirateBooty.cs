﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PiratesLocal2016.ViewModels
{
    public class PirateBooty
    {
        /// <summary>
        /// ViewMoldel to get aggregate data from Pirate and Crew
        /// </summary>
        public int ID { get; set; }
/// <summary>
/// From Pirates Table
/// </summary>
        public string PirateName { get; set; }
/// <summary>
/// From Crews Table
/// </summary>
        public decimal? TotalBooty { get; set; }
    }
}