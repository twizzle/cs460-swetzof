﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChangeMajor.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        /// <summary>
        /// Opens the home page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}