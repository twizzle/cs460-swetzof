﻿using ChangeMajor.DAL;
using ChangeMajor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChangeMajor.Controllers
{
    public class StudentController : Controller
    {
        private StudentContext db = new StudentContext();

        /// <summary>
        /// Get the list of all student requests
        /// </summary>
        /// <returns>returns a view of student requests:  ReviewReqs</returns>

        public ActionResult ReviewReqs()
        {
            return View(db.Students.ToList());
        }

        /// <summary>
        /// 
        /// Get the Change Request View
        /// </summary>
        /// <returns>ChangeReq View</returns>
        [HttpGet]
        public ActionResult ChangeReq()
        {
            return View();
        }

        /// <summary>
        /// Takes the info from the form and saves it to the DB redirects to ReviewReq list page
        /// </summary>
        /// <param name="student"></param>
        /// <returns>ReviewReq View</returns>
        [HttpPost]
        public ActionResult ChangeReq(Student student)
        {
            if (ModelState.IsValid)
            {
                db.Students.Add(student);
                db.SaveChanges();
                return RedirectToAction("ReviewReqs");
            }
            return View(student);
        }
    }
}