﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ChangeMajor.Models;

namespace ChangeMajor.DAL
{
    /// <summary>
    /// Set/Get the Student table from the DB
    /// </summary>
    public class StudentContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
    }
}