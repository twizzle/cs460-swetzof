-- Create tables and populate with seed data
--    follow naming convention: "Users" table contains rows that are each "User" objects

-- ***********  Attach ***********
CREATE DATABASE [StudentManagement] ON
PRIMARY (NAME=[StudentManagement], FILENAME='$(dbdir)\StudentManagement.mdf')
LOG ON (NAME=[StudentManagement_log], FILENAME='$(dbdir)\StudentManagement_log.ldf');
--FOR ATTACH;
GO

USE [StudentManagement];
GO

-- *********** Drop Tables ***********

IF OBJECT_ID('dbo.Students','U') IS NOT NULL
	DROP TABLE [dbo].[Students];
GO


-- ########### Users ###########
CREATE TABLE [dbo].[Students]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[FirstName] NVARCHAR (50) NOT NULL,
	[LastName] NVARCHAR (50) NOT NULL,
	[Date] DATETIME NOT NULL,
	[PhoneNumber] NVARCHAR (15),
	[Email] NVARCHAR (50) NOT NULL,
	[CatalogYear] Int NOT NULL,
	[VNumber] NVARCHAR (11) NOT NULL,
	[Major] NVARCHAR (50) NOT NULL,
	[Minor] NVARCHAR (50) NOT NULL,
	[Advisor] NVARCHAR (50) NOT NULL,
	CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([ID] ASC)
);

BULK INSERT [dbo].[Students]
	FROM '$(dbdir)\SeedData\Student.csv'		-- ID,FirstName,LastName,Date, PhoneNumber, Email, CatalogYear, VNumber, Major, Minor, Advisor
	WITH
	(
		FIELDTERMINATOR = ',',
		ROWTERMINATOR	= '\n',
		FIRSTROW = 2
	);
GO

-- ***********  Detach ***********
USE master;
GO

ALTER DATABASE [StudentManagement] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO

EXEC sp_detach_db 'StudentManagement', 'true'
