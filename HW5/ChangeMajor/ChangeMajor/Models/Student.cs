namespace ChangeMajor.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Student
    {
        /// <summary>
        /// Primary Key in Table
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Student First Name
        /// </summary>
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        /// <summary>
        /// Student Last Name
        /// </summary>
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        /// <summary>
        /// Request Date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Student Phone Number
        /// </summary>
        [StringLength(15)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Student Email Address
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        /// <summary>
        /// Student's Entry Catalog Year
        /// </summary>
        public int CatalogYear { get; set; }

        /// <summary>
        /// Students V#
        /// </summary>
        [Required]
        [StringLength(11)]
        public string VNumber { get; set; }

        /// <summary>
        /// Students current or requested major
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Major { get; set; }

        /// <summary>
        /// Students current or requested minor
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Minor { get; set; }

        /// <summary>
        /// Students Current or request advisor
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Advisor { get; set; }
    }
}
